<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Login Form Template</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

</head>

        <style>
          td{
            text-align: left;
          }
        </style>
    </head>

    <body>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Passwort</th>
                <th scope="col">Rolle</th>
              </tr>
            </thead>
            <tbody>
              <?php
              include "./connection.php";
              if($_POST){
                if(isset($_POST["ChangedPassword"])){
                  //Update password
                  $Passwort = $_POST["ChangedPassword"];
                  $Username = $_POST["form-username"];
                  $stmnt = $link->prepare("UPDATE benutzer SET Passwort=? WHERE Username=?");
                  $stmnt->bind_param("ss", $Passwort, $Username);
                  $stmnt->execute();
                                $result = $stmnt->get_result();
                }else if(isset($_POST["ChangedUsername"])){
                  //update username
                  $newUsername = $_POST["ChangedUsername"];
                  $Username = $_POST["form-username"];
                  $stmnt = $link->prepare("UPDATE benutzer SET Username=? WHERE Username=?");
                                $stmnt->bind_param("ss", $newUsername, $Username);
                                $stmnt->execute();
                                $result = $stmnt->get_result();
                }else if (isset($_POST["Rolle"])){
                  // Update rolle
                  $link->query("UPDATE benutzer SET " .
                      "Rolle ='".$_POST["Rolle"] ."' " .
                      "WHERE Username='".$_POST["form-username"] . "'");
                }else if(isset($_POST["DeleteUser"])){
                  //löschen
                  $link->query("DELETE FROM benutzer WHERE Username='".$_POST["DeleteUser"] . "'");
                }else if(isset($_POST["form-password"])){
                  //einfügen
                  $err = $link->query("INSERT INTO benutzer(Username, Passwort, Rolle) " .
                              " VALUES ('".$_POST["form-username"] ."', '" .$_POST["form-password"] ."', '".$_POST["form-rolle"] ."') ");
                }
              }
              $result = $link->query("SELECT * FROM benutzer");
              if(!$result){
                die("Error fetching users: " . mysqli_error());
              }
              while($row = mysqli_fetch_assoc($result)){
                echo "<tr>";
                echo "<td>";
                echo "<form action='' method='post'>" . 
                    "<input name='DeleteUser' value='" . $row["Username"] . "' type='text' style='display: none;'/>" .
                    "<input type='submit' class='btn btn-danger' value='&times;'/></form>";
                echo "</td>";
                echo "<td>" . $row["Username"] . "<span  data-toggle='modal' data-target='#myModal1' data-bs-whatever='@Username' onclick='changeModal(\"" .  $row["Username"] . "\")'>" .
                "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-pen-fill' viewBox='0 0 16 16'>" .
                "<path d='M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z'/>" .
              "</svg>" .
              "<span class='visually-hidden'></span>" .
              "</span>" ;
              echo "</td>";
                echo "<td>" . $row["Passwort"] . "<span  data-toggle='modal' data-target='#myModal2' data-bs-whatever='@Passwort' onclick='changePwModal(\"" .  $row["Username"] . "\")'>" .
                "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-pen-fill' viewBox='0 0 16 16'>" .
                "<path d='M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z'/>" .
              "</svg>" .
              "<span class='visually-hidden'></span>" .
              "</span>" ;
              echo "</td>";
                echo "<td>" . $row["Rolle"] . "<span  data-toggle='modal' data-target='#myModal3' data-bs-whatever='@Rolle' onclick='changeRankModal(\"" .  $row["Username"] . "\")'>" .
                "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-pen-fill' viewBox='0 0 16 16'>" .
                "<path d='M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z'/>" .
              "</svg>" .
              "<span class='visually-hidden'></span>" .
              "</span>" ;
              echo "</td>";
                echo "</tr>";
              }
              mysqli_free_result($result);
              ?>
            </tbody>
          </table>

<div class="container">
  <h2>Add a User</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal4">New User</button>
  <div class="modal fade" id="myModal4" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <form form action="" method="post">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">New Username:</label>
            <input name='form-username' type="text" class="form-control" id="recipient-name">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">New Passwort:</label>
            <input name='form-password' type="text" class="form-control" id="recipient-name">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">New Rolle:</label>
            <input name='form-rolle' type="text" class="form-control" id="recipient-name">
          </div>
          <br>
          <button type="Submit" class="btn btn-outline-secondary form-control">Erstellen</button>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

  <!-- Modal1 -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <form action="" method="post">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">New Username:</label>
            <input name='ChangedUsername' type="text" class="form-control" id="recipient-name">
          </div>
          <br>
          <button type="Submit" class="btn btn-outline-secondary form-control">Ändern</button>
          <div class="mb-3" style="display:none;">
            <label for="ChangeUser" class="col-form-label">Username:</label>
            <input name="form-username" type="text" id="ChangeUser"/>
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<!-- Modal2 -->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <form action="" method="post">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">New Passwort:</label>
            <input name='ChangedPassword' type="text" class="form-control" id="recipient-name">
          </div>
          <br>
          <button type="Submit" class="btn btn-outline-secondary form-control">Ändern</button>
          <div class="mb-3" style="display:none;">
            <label for="ChangePwUser" class="col-form-label">Username:</label>
            <input name="form-username" type="text" id="ChangePwUser"/>
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<!-- Modal3 -->
<div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <form action="" method="post">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">New Rolle:</label>
            <input name='Rolle' type="text" class="form-control" id="recipient-name">
          </div>
          <br>
          <button type="Submit" class="btn btn-outline-secondary form-control">Ändern</button>
          <div class="mb-3" style="display:none;">
            <label for="ChangeRankUser" class="col-form-label">Username:</label>
            <input name="form-username" type="text" id="ChangeRankUser"/>
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

            <!-- Javascript -->
            <script src="assets/js/jquery-1.11.1.min.js"></script>
            <script src="assets/bootstrap/js/bootstrap.min.js"></script>
            <script src="assets/js/jquery.backstretch.min.js"></script>
            <script src="assets/js/scripts.js"></script>
            <script>
              function changeModal(name){
                document.getElementById('ChangeUser').value = name;
              }
              function changePwModal(name){
                document.getElementById('ChangePwUser').value = name;
              }
              function changeRankModal(name){
                document.getElementById('ChangeRankUser').value = name;
              }
            </script>
            
            <!--[if lt IE 10]>
                <script src="assets/js/placeholder.js"></script>
                
            <![endif]-->
    
        </body>
    
    </html>